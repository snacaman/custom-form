(function( $ ) {

$.fn.customForm = function( method ) {

	var settings = {
		init : false,
		nameSpacer : "-"
	};

	var requiredCss = {
		"-moz-box-sizing" : "border-box",
		"-webkit-box-sizing" : "border-box",
		"box-sizing" : "border-box"
	};

	var constants = {
		types : {
			input : {
				text     : "text",
				password : "text",
				checkbox : "checkbox",
				radio    : "radio",
				submit   : "button",
				reset    : "button",
				hidden   : "hidden"
			},
			textarea : "textarea",
			select : "select",
			button : "button"
		}
	};

	var methods = {
		init: function( userSettings ) {

			settings = $.extend(settings, userSettings);

			// Hide some controls
			$("body").click( function() {
				$(".control.select:not(.active) ul:visible").hide();
			});

			return this.each( function() {

				$(this).addClass("custom-form");
				$(this).find("input[type!=hidden], select, textarea, button").each( function() {

					var $formElement = $(this);
					var $control = $('<div class="control">');

					$formElement.css(requiredCss);
					$control.css(requiredCss);

					switch( getType(this) ) {
						case "select" :

							$formElement.addClass("visuallyhidden");
							$control.addClass("select");

							var formElementOptions = $formElement.find("option");

							var controlId = $formElement.attr("id");
							if( controlId ) $control.attr("id", controlId + settings.nameSpacer + "custom");

							var current = $('<div class="current">');
							var currentText = $('<div class="text">');
							current.append( currentText );

							var options = $("<ul>").css({"z-index" : 1}).hide();

							$(formElementOptions).each(function(idx, val) {

								var option = $("<li>").html( val.innerHTML ).click( function() {
									$(this).addClass("selected").siblings().removeClass("selected");
									$formElement[0].options[idx].selected = true;
									currentText.html( val.innerHTML ).attr("title", val.innerHTML);
									$formElement.change();

								});

								if(idx == $formElement[0].selectedIndex) {
									option.addClass("selected");
									currentText.html( val.innerHTML ).attr("title", val.innerHTML);
								}

								options.append( option );
							});

							$control.click( function() {
								options.toggle();
							});

							$control.mouseover( function() {
								$(this).addClass("active");
							});

							$control.mouseout( function() {
								$(this).removeClass("active");
							});

							$control.append( current ).append( options );
							$formElement.after( $control );

						break;

						case "textarea" :

							$control.addClass("text");

							var controlId = $formElement.attr("id");
							if( controlId ) $control.attr("id", controlId + settings.nameSpacer + "custom");

							$formElement.wrap($control);

						break;

						case "text" :

							$control.addClass("input");

							var controlId = $formElement.attr("id");
							if( controlId ) $control.attr("id", controlId + settings.nameSpacer + "custom");

							$formElement.wrap($control);

						break;

						case "radio" :

							$formElement.addClass("visuallyhidden");

							var radioName = $formElement.attr("name");
							$control.addClass("radio").attr("rel", radioName).data("clickback", true);

							if( $formElement.is(":checked") ) {
								$control.addClass("checked");
							}

							$control.click(function() {
								$controls = $("div[rel=" + radioName + "]").removeClass("checked");
								$(this).addClass("checked");

								if( $(this).data("clickback") ) {
									$formElement.click();
								}

								$(this).data("clickback", true);
							});

							$formElement.click(function() {
								$control.data("clickback", false);
								$control.click();
							});

							$formElement.after($control);

						break;

						case "checkbox" :

							$formElement.addClass("visuallyhidden");

							$control.addClass("checkbox").data("clickback", true);

							if( $formElement.is(":checked") ) {
								$control.addClass("checked");
							}

							$control.click(function() {

								if( $(this).data("clickback") ) {
									if($.browser.msie && parseInt($.browser.version) === 8) {
										// IE 8 Broken "change" event fix
										$formElement.click().change();
									} else {
										$formElement.click();
									}
								}

								$(this).data("clickback", true);

								if($formElement.is(":checked")) {
									$(this).addClass("checked");
								}
								else {
									$(this).removeClass("checked");
								}
							});

							$formElement.click(function() {
								$control.data("clickback", false);
								$control.click();
							});

							$formElement.after($control);

						break;

						default:
					}

				});

				// Apply extra css
				if(settings.css) {
					for (var x in settings.css) {
						$(this).find(x).css(settings.css[x]);
					}
				}

			});
		},
		test: function() {
			if(!settings.init) {
				$.error( "Set up customForm plugin first" );
			}
			return this;
		}
	};

	if( methods[method] ) {
		return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
	}
	else if( typeof method === 'object' || ! method ) {
		return methods.init.apply( this, arguments );
	}
	else {
		$.error( "Method " +  method + " does not exist on customForm plugin." );
	}

	/* Helpers */

	function getType(node) {
		var name = constants.types[ node.nodeName.toLowerCase() ] ;
		if(typeof name == "object") {
			name = constants.types[ node.nodeName.toLowerCase() ][ node.type ];
		}
		return name;
	}

};

})( jQuery );